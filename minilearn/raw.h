#pragma once

namespace minilearn {

template<class SerialT, class MemT>
struct raw_struct { MemT v; };

template<class SerialT, class MemT>
raw_struct<SerialT, MemT> raw(MemT && v)
{   return {std::forward<MemT>(v)};
}

// ostream << minilearn::raw<float>(x)
// writes raw bytes of float(x)
template<class Stream, class SerialT, class MemT>
Stream & operator<<(Stream && s, raw_struct<SerialT, MemT> r)
{   SerialT tmp = SerialT(r.v);
    s.write(reinterpret_cast<const char*>(&tmp), sizeof(tmp));
    return s;
}

// istream >> minilearn::raw<float>(x)
// reads raw bytes float, and assigns to x
template<class Stream, class SerialT, class MemT>
Stream & operator>>(Stream && s, raw_struct<SerialT, MemT> r)
{   SerialT tmp;
    s.read(reinterpret_cast<char*>(&tmp), sizeof(tmp));
    r.v = tmp;
    return s;
}





// template<class S, class T>
// raw_wrapper<S> operator<<(raw_wrapper<S> s, T const& obj)
// {   s.s.write(reinterpret_cast<const char*>(&obj), sizeof(T));
//     return s;
// }

// // istream >> minilearn::raw<float>(x)
// // reads raw bytes float, and assigns to x
// template<class Stream>
// raw_wrapper<Stream> operator>>(Stream && s, raw r) { return {s}; }

// template<class S, class T>
// raw_wrapper<S> operator>>(raw_wrapper<S> s, T & obj)
// {   s.s.read(reinterpret_cast<char*>(&obj), sizeof(T));
//     return s;
// }





// template<class Stream>
// struct raw_wrapper { 
//     Stream & s; 
//     operator bool() const { return (bool)s; }
// };

// // ostream << minilearn::raw() << x
// // writes raw bytes of x
// template<class Stream>
// raw_wrapper<Stream> operator<<(Stream && s, raw r) { return {s}; }

// template<class S, class T>
// raw_wrapper<S> operator<<(raw_wrapper<S> s, T const& obj)
// {   s.s.write(reinterpret_cast<const char*>(&obj), sizeof(T));
//     return s;
// }

// // istream >> minilearn::raw() >> x
// // reads raw bytes of x
// template<class Stream>
// raw_wrapper<Stream> operator>>(Stream && s, raw r) { return {s}; }

// template<class S, class T>
// raw_wrapper<S> operator>>(raw_wrapper<S> s, T & obj)
// {   s.s.read(reinterpret_cast<char*>(&obj), sizeof(T));
//     return s;
// }

} // minilearn