#pragma once

#include "minilearn/vec.h"

namespace minilearn {

template<class Mat>
using mat_elem_t = std::remove_reference_t<decltype(declval<Mat>()(0,0))>;

template<class Mat>
using mat_rows_t = decltype(declval<Mat>().rows());

template<class Mat>
using mat_cols_t = decltype(declval<Mat>().cols());

template<class Src, class Dst>
void mat_copy(Src && src, Dst && dst)
{   assert(dst.rows() == src.rows());
    assert(dst.cols() == src.cols());
    for(int r=0 ; r<dst.rows() ; r++)
    for(int c=0 ; c<dst.cols() ; c++)
    {   dst(r,c) = src(r,c);
    }
}
template<class Mat, 
    class RowT = mat_rows_t<Mat>, class ColT = mat_cols_t<Mat>>
void exec(Mat && src)
{   for(RowT r=0 ; r<src.rows() ; r++)
    for(ColT c=0 ; c<src.cols() ; c++)
    {   src(r,c);
    }
}

template<class T>
struct mat
{
    vec<T> _data;
    int _rows;
    int _cols;

    mat(int irows, int icols)
    :   _data(irows * icols),
        _rows(irows),
        _cols(icols)
    {   
    }
    mat(mat const& o) = default;
    T & operator()(int r, int c)
    {   return _data[r * _cols + c];
    }
    T const& operator()(int r, int c) const
    {   return _data[r * _cols + c];
    }
    int rows() const { return _rows; }
    int cols() const { return _cols; }
    void resize(int r, int c)
    {   _data.resize(r * c);
        _rows = r;
        _cols = c;
    }

    mat & operator=(mat && o) = default;
    mat & operator=(mat const& o) = default;

    template<class Mat>
    mat & operator=(Mat && o)
    {   resize(o.rows(), o.cols());
        mat_copy(o, *this);
        return *this;
    }

    template<class Mat>
    struct row_t
    {   Mat & m;
        int r;
        auto & operator[](int c) { return m(r,c); }
        int size() const { return m.cols(); }
    };
    row_t<const mat> row(int r) const { return {*this, r}; }
    row_t<mat> row(int r) { return {*this, r}; }

    template<class Mat>
    struct col_t
    {   Mat & m;
        int c;
        auto & operator[](int r) { return m(r,c); }
        int size() const { return m.rows(); }
    };
    col_t<const mat> col(int c) const { return {*this, c}; }
    col_t<mat> col(int c) { return {*this, c}; }
};

// map(mat, f(x))
template<class M, class F, class R = decltype(declval<F>()(declval<M>()(0,0)))>
struct mat_map
{   M m; F func;
    int rows() const { return m.rows(); }
    int cols() const { return m.cols(); }
    R operator()(int r, int c) { return func(m(r, c)); }
};
template<class M, class F>
mat_map<M, F> map(M && m, F && f)
{   return { std::forward<M>(m), std::forward<F>(f) };
}

// mat * vec
template<class Mat, class Vec, class R = sum_t<mat_elem_t<Mat>, vec_elem_t<Vec>>>
struct mat_vec_prod
{   Mat m; Vec v;
    int size() const { return m.rows(); }
    R operator[](int i) { return dot(m.row(i), v); }
};
template<class Mat, class Vec>
mat_vec_prod<Mat, Vec> operator*(Mat && m, Vec && v)
{   return { std::forward<Mat>(m), std::forward<Vec>(v) };
}

// vec * mat
template<class Mat, class Vec, class R = sum_t<mat_elem_t<Mat>, vec_elem_t<Vec>>>
struct vec_mat_prod
{   Mat m; Vec v;
    int size() const { return m.cols(); }
    R operator[](int i) { return dot(m.col(i), v); }
};
template<class Mat, class Vec>
vec_mat_prod<Mat, Vec> operator*(Vec && v, Mat && m)
{   return { std::forward<Mat>(m), std::forward<Vec>(v) };
}

template<class Stream, class Mat, 
    class ElemT = mat_elem_t<Mat>,
    class RowsT = decltype(declval<Mat>().rows()),
    class ColsT = decltype(declval<Mat>().cols())>
Stream & operator<<(Stream && out, Mat const& m)
{   out << "[";
    for(int r=0 ; r<m.rows() ; r++)
    {   out << (r ? ",\n" : "") << "[";
        for(int c=0 ; c<m.cols() ; c++)
        {   out << (c ? ", " : "") << m(r, c);
        }
        out << "]";
    }
    return out << "]";
}


} // minilearn